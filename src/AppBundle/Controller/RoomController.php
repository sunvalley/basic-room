<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/room")
 */
class RoomController extends Controller
{
    /**
     * @Route("/", methods={"GET"})
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Room:index.html.twig', array());
    }

    /**
     * @Route("/heartbeat", methods={"POST"}, condition="request.isXmlHttpRequest()")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function heartBeatAction(Request $request)
    {
        return new JsonResponse([]);
    }

    /**
     * @Route("/enter/{userhandle}", methods={"POST"}, condition="request.isXmlHttpRequest()")
     *
     * @param $userhandle
     * @return JsonResponse
     */
    public function enterAction($userhandle)
    {
        return new JsonResponse(false);
    }
}
