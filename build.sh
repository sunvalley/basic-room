#!/usr/bin/env bash

# Usage: ./build.sh [skip-test]

WORK_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

DO_TEST=true
if [[ "$1" == "skip-test" ]]; then
    DO_TEST=false
fi

cd "$WORK_DIR"
export APP_ENV=dev
# install with dev packages first
if ! composer install; then
    exit 1
fi

if $DO_TEST; then
    # run unit tests
    if ! php vendor/bin/phpunit --configuration "$WORK_DIR/phpunit.xml.dist"; then
        exit 1
    fi
else
    echo "Testing skipped due to user command"
fi

# remove node_modules and all cache
rm -fr var/cache/test var/cache/dev var/cache/prod
rm -fr web/app_dev.php web/favicon.ico web/apple-touch-icon.png web/config.php

# remove dev packages
export APP_ENV=prod
composer install --no-dev --optimize-autoloader

php bin/console cache:warmup --env=prod
php bin/console assets:install --env=prod
