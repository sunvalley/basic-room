#### REQUIREMENTS

##### INTERFACES

1. Login interface, where user enters its handle to enter to the room
where User handle is;

    - 20 characters
    - [a-zA-Z0-9_-]

2. Room view: List of (active) people and (Optional) Showing how long they have been in the room



##### TECHNICAL DOC

Communication Protocol: JSON

**Endpoint GET /**				
Accept: text/html
Content-Type: text/html
Renders the single page application

**Endpoint POST /room/enter/{userhandle}**	
Accept: application/json
Content-Type: application/json
User enters to the room, while on the entering stage set a cookie to the browser so that you bind the browser to that particular userhandle. Don’t allow two of the same user handles at the same room.

**Endpoint POST /room/heartbeat**     	
Accept: application/json 
Content-Type: application/json
Informs the backend that the user is still active


##### PROTOCOL FORMAT

[
{nickname: "foo", timestamp:124312412412},
{nickname: "bar", timestamp:124312412412},
{nickname: "baz", timestamp:124312412412},
{nickname: "qux", timestamp:124312412412}
]


      
